// Variable Blink
// Written by: Thomas Tongue
// Date: July 20th, 2015

int LED_A = 2;              // LED_A GPIO Pin
int brightness_A = 0;       // Brightness of the LED_A

int LED_B = 4;              // LED_B GPIO Pin
int brightness_B = 1024;       // Brightness of the LED_B

int brightnessChange = 10; // Amount that the brightness will change

void setup() { 
  pinMode(LED_A,OUTPUT);    // set the LED_A pin to be an output
  pinMode(LED_B,OUTPUT);    // set the LED_B pin to be an output  
}

void loop() {
  // set the value of the LED_A pin, which will set the LED_A brightness
  analogWrite(LED_A,brightness_A);
  
  // change the brightness for the next time through the loop
  brightness_A=brightness_A+brightnessChange;
  // Use the modulus operator to keep brightness in the 0 - 1023 range
  brightness_A = brightness_A % 1024;

  // set the value of the LED_A pin, which will set the LED_A brightness
  analogWrite(LED_B,brightness_B);
  
  // change the brightness for the next time through the loop
  brightness_B=brightness_B-brightnessChange;
  // Use the modulus operator to keep brightness in the 0 - 1023 range
  brightness_B = brightness_B % 1024;

  // wait a little before doing this loop over.
  delay(30);
}

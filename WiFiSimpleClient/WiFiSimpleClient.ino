// WiFi Simple Client
// Written by: Thomas Tongue
// Date: July 20th, 2015

#include <ESP8266WiFi.h>

const char* ssid     = "mak3rs oneplus";
const char* password = "truck2blue9challenge";

const char* host = "esp8266.tungadyn.com";
const char* path = "/wificlient/time.php";
const int httpPort = 80;

//number of expected blank lines after the header
const int contentBreakCnt = 1;
//flag to idenitify when the header is finished
int headerComplete = 0;

void setup() {
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void loop() {
  delay(5000);

  Serial.print("connecting to ");
  Serial.println(host);
  
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }
  
  Serial.print("Requesting URL: ");
  Serial.println(path);
  
  // This will send the request to the server
  client.print(String("GET ") + path + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");
  delay(1000);
  
  // Read all the lines of the reply from server and print 
  // them to Serial
  //keep track of the lines of whitespace after the header
  int cnt = 0;
  while(client.available()){
    String line = client.readStringUntil('\r');
    //Count up until we have completed the header, then allow blank lines in the actual content (is this even allowable in http 1.1?)
    if (!headerComplete) {
      if (line.length() == 1) {
        cnt++;        
      } 

      if (cnt == contentBreakCnt) {
        headerComplete = 1;
      }
      //Keep reading 
      continue;
    }
    Serial.print(line);
  }
  
  headerComplete = 0;
  Serial.println();
  Serial.println("closing connection");

}


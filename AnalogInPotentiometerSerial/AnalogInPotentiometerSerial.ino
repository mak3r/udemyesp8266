// Analog In Potentiometer Serial
// Written by: Thomas Tongue
// Date: July 20th, 2015

int LED_A = 2;              // LED_A GPIO Pin
int brightness_A = 0;       // Brightness of the LED_A

void setup() {
  // Initialize the Serial port 
  Serial.begin(115200);
  // Note: No need to set the pin mode for A0,
  // It's INPUT by default!
  pinMode(LED_A,OUTPUT);    // set the LED_A pin to be an output
}

void loop() {
  //This is not really 10Hz (10 samples per second)
  //Programmatically, it would make more sense to not use the delay below
  // and instead, calculate the Hz desired and use that as an interval to 
  // capture ADC0. 
  int sum = 0;
  for (int i=0; i<10; i++) {
    sum += analogRead(A0);
  }

  brightness_A = sum/10;
  
  // Every second provide the raw value of the A0 input
  Serial.println(String(brightness_A/1024.0,DEC) + "V");  
  delay(1000);

  analogWrite(LED_A,brightness_A);  
}

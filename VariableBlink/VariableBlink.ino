// Variable Blink
// Written by: Thomas Tongue
// Date: July 10th, 2015

int LED=2;         // LED GPIO Pin
int waitTime=100;  // How long to wait between blinks
  
void setup() {                
  // initialize the digital pin as an output.
  pinMode(LED, OUTPUT);     
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(LED, HIGH);   // turn the LED on 
  delay(waitTime);               // wait 
  digitalWrite(LED, LOW);    // turn the LED off 
  delay(waitTime);               // wait
  
          // Set the wait time to the modulus of its current value plus
          // a step value. This will result in wait times that range from
          // 100 to 1000 in steps of 100.
  waitTime=(waitTime % 1000)+100; 
}

// Digital Input Button
// Written by: Mark Abrams
// Date: January 26th, 2016

const int LED = 2;
const int buttonPin = 0;     // the number of the button which determines how long the LED is on
int buttonState=0;
volatile int triggerDetected = 0;
unsigned long startTime = 0; //when to start clocking the interval
unsigned long interval = 0; //interval set by the button
unsigned long cur = 0; //current time
unsigned long prev = 0; //store last time led was updated

int ledState = LOW;

void setup() {
  Serial.begin(115200);
  Serial.println("initializing");
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
  // initialize the LED pin as output
  pinMode(LED, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(buttonPin), pushedButtonEvent, FALLING);
}

void loop() {  
    cur = millis();

    if (triggerDetected == true) {
      Serial.println("Last duration was " + String(interval,DEC));
      Serial.flush();
      Serial.println("Button pressed");
      Serial.flush();
      interval = 0;
      startTime= millis();
      Serial.println("Start time is: " + String(startTime,DEC));
      Serial.flush();
      triggerDetected = 0;
    }
    
    // read the state of the pushbutton value:
    buttonState = digitalRead(buttonPin);
    if (buttonState == LOW) {
      //Capture how long it's been since the start time was triggered
      interval = millis() - startTime;
    } 

    if (cur - prev >= interval) {
      prev = cur;

      if (ledState == HIGH) {
        ledState = LOW;
      } else {
        ledState = HIGH;
      }
      digitalWrite(LED, ledState);   // toggle the LED state 
    }


}

static void pushedButtonEvent() {
  triggerDetected = 1;
}




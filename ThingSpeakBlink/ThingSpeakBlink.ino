// ThingSpeak Updater
// Written by: Thomas Tongue
// Date: July 21st, 2015

#include <ESP8266WiFi.h>

const char* ssid     = "mak3rs oneplus";
const char* password = "truck2blue9challenge";
String writeAPIKey   = "1GPMHEKF4WUXECZS";

const char* thingSpeakAddress = "api.thingspeak.com";
const int updateThingSpeakInterval = 16*1000;

long lastConnectionTime = 0;
boolean lastConnected = false;
int failedCounter=0;

/////// Interrupt variables //////////////////
const int LED = 2;
const int buttonPin = 0;     // the number of the button which determines how long the LED is on
int buttonState=0;
volatile int triggerDetected = 0;
unsigned long startTime = 0; //when to start clocking the interval
unsigned long interval = 100; //interval set by the button
unsigned long cur = 0; //current time
unsigned long prev = 0; //store last time led was updated
signed long ledStateToggled = 0; //number of times the light was toggled.

unsigned long last_interval = 0;
int ledState = LOW;
/////////////////////////////////////////////

void updateThingSpeak(String tsData);
static void pushedButtonEvent() {
  triggerDetected = 1;
}

void setup() {
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("\n\nConnecting to ");
  Serial.println(ssid); 
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected. IP address: "+WiFi.localIP());

  //Setup IO pins for interrupt handling
  pinMode(buttonPin, INPUT);
  // initialize the LED pin as output
  pinMode(LED, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(buttonPin), pushedButtonEvent, CHANGE);
}


void loop() {
  cur = millis();
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  if (triggerDetected == true && buttonState == HIGH) {
    triggerDetected = 0;    
    Serial.println("Last duration was " + String(interval,DEC));
    Serial.flush();
    Serial.println("Button pressed");
    Serial.flush();

    //Capture how long it's been since the start time was triggered
    interval = millis() - startTime;
  }
  
  if (triggerDetected == true && buttonState == LOW) {
    triggerDetected = 0;
    startTime= millis();
    Serial.println("Start time is: " + String(startTime,DEC));
    Serial.flush();
  } 

  //When the interval has expired toggle the state of the LED
  if (cur - prev >= interval) {
    prev = cur;

    if (ledState == HIGH) {
      ledState = LOW;
    } else {
      ledState = HIGH;
    }
    ledStateToggled++;
    digitalWrite(LED, ledState);   // toggle the LED state 
  }

  //interval changed, post it
  if ((WiFi.status() == WL_CONNECTED) && (last_interval != interval)) {
    updateThingSpeak("field1="+String(interval/1000.0,DEC)+"&field2="+String(ledStateToggled,DEC)+"&field3="+String(cur,DEC));
    last_interval = interval;
    ledStateToggled = 0;
  }
}


void updateThingSpeak(String tsData)
{
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(thingSpeakAddress, httpPort)) {
    failedCounter++;
    Serial.println("Connection to ThingSpeak Failed ("+String(failedCounter, DEC)+")");   
    Serial.println();
    
    lastConnectionTime = millis(); 
    return;
  } 
  client.print("POST /update HTTP/1.1\n");
  client.print("Host: api.thingspeak.com\n");
  client.print("Connection: close\n");
  client.print("X-THINGSPEAKAPIKEY: "+writeAPIKey+"\n");
  client.print("Content-Type: application/x-www-form-urlencoded\n");
//  client.print("Content-Type: application/json");
  client.print("Content-Length: ");
  client.print(tsData.length());
  client.print("\n\n");

  client.print(tsData);
    
  lastConnectionTime = millis();
    
  if (client.connected())
  {
      delay(10);
      Serial.println("Connecting to ThingSpeak...");
      Serial.println();
      
      failedCounter = 0;
      while(client.available()){
        String line = client.readStringUntil('\r');
        Serial.print(line);
      }
  
      Serial.println();
      Serial.println("closing connection");
  }
  else
  {
      failedCounter++;
  
      Serial.println("Connection to ThingSpeak failed ("+String(failedCounter, DEC)+")");   
      Serial.println();
  }
}




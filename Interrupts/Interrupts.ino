/*
  Interrupts

 Prints the button state on GPIO0 to Serial.

 The circuit:
 * pushbutton attached to GPIO0 from with 
   a 1K resistor pullup to Vcc
 * Other side of button connected to ground
 */

// constants won't change. They're used here to
// set pin numbers:
const int buttonPin = 12;     // the number of the pushbutton pin
volatile int triggerDetected=0;
int loopCnt = 0;

void setup() {
  Serial.begin(115200);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
  attachInterrupt(buttonPin,pushedButtonEvent,RISING);
}

void loop() {
  if (triggerDetected) {
    Serial.println(".");
    Serial.println("looped " + String(loopCnt,DEC) + " times.");
    loopCnt = 0;
    triggerDetected=0;
  }

  int pinState = digitalRead(buttonPin);
  if (pinState == LOW) {
    loopCnt++;
    Serial.print(".");
  }
}

static void pushedButtonEvent() {
  triggerDetected=1;
}
